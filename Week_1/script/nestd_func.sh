#!/bin/bash

# Nested function 

Gender=$1

funcHuman () {
	Arms=2
	Legs=2

	funcMale() {
		Beard=100
		echo "Man has $Arms and $Legs with $Beard beards"
	}
	
	funcFemale() {
		Beard=0
		echo "Female has $Arms and $Legs with $Beard beards"
	}
}

echo "Enter the gender $Gender"

if [ "$Gender" == "male" ]; then
	funcHuman
	funcMale
else
	funcHuman
	funcFemale
fi

