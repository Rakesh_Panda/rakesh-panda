#!/bin/bash

# Read and write in a file using file descriptors and handlers

echo "Enter a file :"
read FILE

exec 4<>$FILE

while read -r superhero; do
	echo "super hero name = : $superhero"
done <&4

echo "Date and time: `date`" >&4

exec 4>&~


