#!/bin/bash

# If Else and elseif statement

echo "Enter a number between 1 to 3:"
read NUM

if [ $NUM -eq 1 ] 2> /dev/null
then
	echo "You guess the correct number"
elif [ $NUM -eq 2 ] 2> /dev/null
then
	echo "You guess the correct number"
elif [ $NUM -eq 3 ] 2> /dev/null
then
	echo "You guess the correct number"
else
	echo "You did not follow the instructions"
fi
