#!/bin/bash

# Implimentaion of error handling with exit code 143

echo "Enter directory"
DIR=$1

cd $DIR 2>/dev/null

if [ "$?" -eq 0 ]; then
	echo "Change directory $DIR"
	echo "`ls -al`"
else
	echo "Can not change directory"
	exit 143
fi
