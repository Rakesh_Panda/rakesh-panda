#!/bin/bash

# Interracting scripting using Read command

echo "Enter Your First Name:"
read FIRSTNAME
echo "Enter Your Last Name:"
read LASTNAME

echo "Your Name is : $FIRSTNAME $LASTNAME"

echo "Enter Your age :"
read USERAGE

echo "After 10 years. Your age will be `expr $USERAGE + 10` Years"
