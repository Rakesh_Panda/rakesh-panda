#!/bin/bash

# menu for simple dialogbox


MENUBOX=${MENUBOX=dialog}

funcDisplaydialogmenu () {
	$MENUBOX --title "[ MAIN MENU]" --menu "Use UP/DOWN arrow to move" 15 45 4 1 "Hello" 2 "world" 3 "Welcome" X "Exit" 2>tmp.txt
}

funcDisplaydialogmenu

case "`cat tmp.txt`" in
	1) echo "Hello";;
	2) echo "world";;
	3) echo "Welcome";;
	X) echo "Exit";;
esac
	
