#!/bin/bash

# Implimentation with simple infobox with warning message

INFOBOX=${INFOBOX=dialog}
TITLE="Default"
MESSAGE="Welcome to ITT"
XCOORD=10
YCOORD=20

funcDis_info_box () {
	$INFOBOX --title "$1" --infobox "$2" "$3" "$4"
	sleep "$5"
}

if [ "$1" == "shutdown" ]; then
	funcDis_info_box "Warning" "We are Shutting down the system" "11" "21" "5"
	echo "Shutting down"

else
	funcDis_info_box "Information" "Hello" "11" "21" "5"
	echo "Hello welcome"
fi

