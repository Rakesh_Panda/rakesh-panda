#!/bin/bash

#Implement input dialog box

INPUTBOX=${INPUTBOX=dialog}
TITLE="Default"
MESSAGE="Message Display"
XCOORD=10
YCOORD=20

funcDisplayinputbox () {
	$INPUTBOX --title "$1" --inputbox "$2" "$3" "$4" 2>tempfile.txt
}


funcDisplayinputbox "Display file name" "File in current directory" "10" "20"

if [ "`cat tempfile.txt`" != "" ]; then
	cat "`cat tempfile.txt`"
else
	echo "Do nothing"
fi
