#!/bin/bash

# Implimentation of while loop for printing a message for 10 times

echo "Enter a number of times to display message:"
read Num
i=1
while [ $i -le $Num ]
do
	echo "Welcome to IN Time Tec"
	i=`expr $i + 1`
done

