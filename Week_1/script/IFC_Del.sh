#!/bin/bash

# Implementation of IFS and delimiting


echo "Enter a file to read :"
read FILE

echo "Enter the delimiter"
read Del

IFS="$Del"

while read -r CPU MEMORY DISK; do
	echo "CPU : $CPU"
	echo "Memory : $MEMORY"
	echo "Disk : $DISK"
done <"$FILE"
