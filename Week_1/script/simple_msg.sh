#1/bin/bash
# Demo of a message box an OK button

#Global variable / default values  

MSGBOX=${MSGBOX=dialog}
TITLE="Default"
MESSAGE="Some Message"
XCOORD=10
YCOORD=20

# Function declearation

funcDisplayMsgbox () {
	$MSGBOX --title "$1" --msgbox "$2" "$3" "$4"
}

# Script
if [ "$1" == "shutdown" ]; then
	funcDiplayMsgbox "WARNING!" "Please press OK when you ready to shutdownthe system" "10" "20"
       echo "SHUTTING DOWN NOW!"
else
	funcDisplayMsgbox "Boring..." "You are not asking for anything fun.." "10" "20"
	echo "Nothing Fun" 
fi 
