#!/bin/bash

# Displaying variable scope in function

Glo_var="Globally Visible"

funcexam () {
	Loc_var="Locally visible"
	echo "Local varable is $Loc_var"
}

echo "Before function Call"

echo "Global variable is $Glo_var"
echo "Local variable is $Loc_var"

echo "After calling the function"

funcexam 

echo "Global variable is $Glo_var"
echo "Local variable is $Loc_var"



