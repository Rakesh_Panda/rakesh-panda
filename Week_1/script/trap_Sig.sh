#!/bin/bash

# Implimentation of trap and signals to avoid kill, CTRL-C, CTRL-Z command

trap 'echo " - Press Q to Exit "' SIGINT SIGTERM SIGTSTP

while [ "$Choice" != "Q" ] && [ "$Choice" != "q" ]; do
	echo "1) One"
	echo "2) Two"
	echo "3) Three"
	echo "Q) Quit"
	read Choice
done
